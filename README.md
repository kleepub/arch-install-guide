# [arch-install-guide](https://wiki.archlinux.org/index.php/installation_guide#Pre-installation)

personal arch instsall guide

## preparing usb drive


### linux

```
dd bs=4M if=/path/to/archlinux.iso of=/dev/sdx status=progress oflag=sync
```


### windows

https://rufus.akeo.ie/


## pre chroot


### check boot mode

efi?
```
ls /sys/firmware/efi/efivars
```


### network

dhcpd is enabled in archiso.
test connection with ping. if ping failed, setup network with netctl
```
cp /etc/netcl/examples/ethernet-static /etc/netctl/
vim /etc/netctl/ethernet-static
# modify file as needed
netctl start ethernet-static
ping google.com
```


### time

```
timedatectl set-ntp true
```


### partitoning

lvm is the king

efi:
```
fdisk /dev/disk
    g
    n
    <enter>
    <enter>
    <enter>
    +550M
    t
    1
    n
    <enter>
    <enter>
    <enter>
    t
    2
    31
    w
mkfs.fat -F32 /dev/sdxY
vgcreate <volume_group> /dev/sda2 #e.g archlvm
lvcreate <size> <volume_group> -n <logical_volume> #e.g -L 2G archlvm swap
lvcreate <size> <volume_group> -n <logical_volume> #e.g -L 12G archlvm home
lvcreate <size> <volume_group> -n <logical_volume> #e.g -l 100%FREE archlvm root
mkfs.xfs /dev/<volume_group>/<logical_volume>
mkswap /dev/<volume_group>/<logical_volume>
swapon /dev/<volume_group>/<logical_volume>

mount /dev/<volume_group>/<logical_volume> /mnt #root lvm
mkdir /mnt/boot
mount /dev/sdxY /mnt/boot #boot partition
```


## installation


### install base packages

```
vim /etc/pacman.d/mirrorlist #put wanted mirrors on top
pacstrap /mnt base base-devel
```


### fstab

```
genfstab -U /mnt >> /mnt/etc/fstab #use uuids
```

## chroot

```
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
hwclock --systohc
vi /etc/locale.gen
#uncomment wanted languages, e.g en_US.UTF-8 UTF-8
locale-gen
vi /etc/locale.conf
#LANG=en_US.UTF-8
vi /etc/hostname
#hostname
vi /etc/hosts
#127.0.0.1	localhost
#::1		localhost
#127.0.1.1	hostname.localdomain	hostname
vi /etc/mkinitcpio.conf
#HOOKS=(base __udev__ ... block __lvm2__ filesystems)
pacman -S lvm2
mkinitcpio -p linux
passwd root
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
grub-mkconfig -o /boot/grub/grub.cfg
exit
umount -R /mnt
reboot
```

done!

see [arch-workstation](https://gitlab.com/mclover/arch-workstation) for installing software with ansible